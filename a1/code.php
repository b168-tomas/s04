<?php
class Property {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
};

class Condominium extends Property {
    public function getName(){
        return $this->name;
    }

    public function getFloors(){
        return $this->floors;
    }

    public function getAddress(){
        return $this->address;
    }

    public function setName($name){
        $this->name = $name;
    }
};

class Building extends Property {
    public function getName(){
        return $this->name;
    }

    public function getFloors(){
        return $this->floors;
    }

    public function getAddress(){
        return $this->address;
    }
    
    public function setName($name){
        $this->name = $name;
    }
};

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');