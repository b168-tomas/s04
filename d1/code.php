<?php

/*   3 Types of Access Modifiers

    1. public - the property or method can be accessed from everywhere. This is default. 
    
    2. protected - the property or method can be accessed within the class and by classes derived from that class
    
    3. private - the property or method can ONLY be accessed within the class */

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
};

// “Getters” and “Setters” are object methods that allows you to control access to a certain class variables / properties. Sometimes, these functions are referred to as “mutator methods”. A “getter” allows to you to retrieve or “get” a given property. A “setter” allows you to “set” the value of a given property.

class Condominium extends Building {
    // Encapsulation - getter
    /* gets certaion value from the  */
    public function getName(){
        return $this->name;
    }

    // Encapsulation - setter
    public function setName($name){
        $this->name = $name;
    }
};

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');